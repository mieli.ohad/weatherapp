import unittest
import datetime
import calendar
from main import weather_app,weekday_from_date, edit_date, edit_time, get_data_from_api, filter_response, page_not_found, internal_server_error


class TestWeatherApp(unittest.TestCase):

    def test_weekday_from_date(self):
        self.assertEqual(weekday_from_date("24", "04", "2023"), "Monday")

    def test_edit_date(self):
        self.assertEqual(edit_date("2023-04-24T08:00:00+00:00"), "24/04/2023")

    def test_edit_time(self):
        self.assertEqual(edit_time("2023-04-24T08:00:00+00:00"), "08:00")

    def test_get_data_from_api(self):
        response = get_data_from_api("New York, USA")
        self.assertEqual(response.status_code, 200)
        self.assertIn("New York", response.text)





if __name__ == '__main__':
    unittest.main()
